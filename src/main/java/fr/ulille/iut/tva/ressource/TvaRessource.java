package fr.ulille.iut.tva.ressource;

import fr.ulille.iut.tva.dto.DetailDTO;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
        return TauxTva.NORMAL.taux;
    }
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux;
        } catch (Exception e) {
            throw new NiveauTvaInexistantException();
        }
    }
    @GET
    @Path("{niveauTva}")
    public double getMontantTotal(@PathParam("niveauTva") String taux , @QueryParam("somme") int somme){
        try {
        return  calculTva.calculerMontant(TauxTva.fromString(taux.toUpperCase()),somme);
        }catch (Exception e){
            throw new NiveauTvaInexistantException();
        }
    }
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public ArrayList<InfoTauxDto> getInfoTaux(){
        ArrayList<InfoTauxDto> result=new ArrayList<>();
        for(TauxTva t:TauxTva.values()){
            result.add(new InfoTauxDto(t.name() , t.taux));
        }
        return result;
    }
    @GET
    @Path("detail/{taux}")
    public DetailDTO getDetail(@PathParam("taux") String taux , @QueryParam("somme") double somme){

        return new DetailDTO(somme,taux);
    }
}
