package fr.ulille.iut.tva.dto;

import fr.ulille.iut.tva.service.TauxTva;

public class DetailDTO {
    private double montantTotal;
    private double montantTva;
    private double somme;
    private String tauxLabel;
    private double tauxValue;

    public DetailDTO(){}

    public DetailDTO(double somme , String tauxLabel ){
        getTaux(tauxLabel);
        getMontant(somme);
    }

    public double getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(double montantTotal) {
        this.montantTotal = montantTotal;
    }

    public double getMontantTva() {
        return montantTva;
    }

    public void setMontantTva(double montantTva) {
        this.montantTva = montantTva;
    }

    public double getSomme() {
        return somme;
    }

    public void setSomme(double somme) {
        this.somme = somme;
    }

    public String getTauxLabel() {
        return tauxLabel;
    }

    public void setTauxLabel(String tauxLabel) {
        this.tauxLabel = tauxLabel;
    }

    public double getTauxValue() {
        return tauxValue;
    }

    public void setTauxValue(double tauxValue) {
        this.tauxValue = tauxValue;
    }

    private void getMontant(double somme){
        this.somme=somme;
        this.montantTva=(this.tauxValue / 100 ) * somme;
        this.montantTotal=somme + montantTva;
    }
    private void getTaux(String tauxlabel) {
        TauxTva taux=TauxTva.valueOf(tauxlabel.toUpperCase());
        this.tauxLabel=taux.name();
        this.tauxValue=taux.taux;
    }
}
