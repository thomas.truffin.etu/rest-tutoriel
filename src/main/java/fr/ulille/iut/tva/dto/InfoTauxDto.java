package fr.ulille.iut.tva.dto;

import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.ArrayList;
@XmlRootElement
public class InfoTauxDto {
    private String label;
    private double taux;

    public InfoTauxDto(){}

    public InfoTauxDto(String label , double taux){
        this.label=label;
        this.taux=taux;
    }
    public String getLabel(){
        return label;
    }
    public double getTaux(){
        return taux;
    }
    public void setLabel(String label){
        this.label=label;
    }
    public void setTaux(double taux){
        this.taux=taux;
    }
}
